use bevy::prelude::*;
use rand::Rng;

use crate::{GameTextures, components::PointsText, PlayerState};


pub struct HUDPlugin;

impl Plugin for HUDPlugin {
	fn build(&self, app: &mut App) {
		app
        .add_startup_system_to_stage(StartupStage::PostStartup, spawn_text)
        .add_system(text_update_system);
	}
}

fn spawn_text(mut commands: Commands, game_resources: Res<GameTextures>) {
    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "Points: ",
                TextStyle {
                    font: game_resources.hud_text_font.clone(),
                    font_size: 21.0,
                    color: Color::WHITE,
                },
            ),
            TextSection::from_style(TextStyle {
                font: game_resources.hud_points_text_font.clone(),
                font_size: 21.0,
                color: Color::GOLD,
            }),
        ])
        .with_style(Style {
            position_type: PositionType::Absolute,
            position: UiRect {
                top: Val::Px(15.0),
                left: Val::Px(15.0),
                ..default()
            },
            ..default()
        }),        
        PointsText,
    ));
}


fn text_update_system(mut query: Query<&mut Text, With<PointsText>>, game_state: Res<PlayerState>) {
    let points_value = game_state.points.floor();
    for mut text in &mut query {
        text.sections[1].value = format!("{points_value:.0}");
    }
}